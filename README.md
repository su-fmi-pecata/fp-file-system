# FP File System

Project for FMI CS FP course

Task can be found in [Task.pdf](https://gitlab.com/su-fmi-pecata/fp-file-system/blob/master/Task.pdf) or [Task.txt](https://gitlab.com/su-fmi-pecata/fp-file-system/blob/master/Task.txt).

This repo contains 2 implementations of FileSystems.\
In [Filesystem.hs](https://gitlab.com/su-fmi-pecata/fp-file-system/blob/master/FileSystem.hs) fs is created using tree.\
In [BetterFS.hs](https://gitlab.com/su-fmi-pecata/fp-file-system/blob/master/BetterFS.hs) fs is created using list with full path of files and folder.