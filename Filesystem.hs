{-# LANGUAGE BlockArguments #-}

import Data.List
import Data.Maybe
import Data.String

-- ################################################################################
-- ## Data types
-- ################################################################################

data File = File {
    fileName :: String,
    fileContent :: String
} deriving (Show, Eq)

data Folder = Folder {
    folderName :: String,
    subFolders :: [Folder],
    subFiles :: [File]
} deriving (Show, Eq)

data FileSystem = FileSystem {
    current :: String,
    root :: Folder
} deriving (Show, Eq)

data CommandResult = CommandResult {
    fileSystem :: FileSystem,
    output :: String
} deriving (Show, Eq)

data Command = Pwd 
    | Ls  { dir :: String } -- dir is "" or relative/path/ or /full/path/
    | Cd  { path :: String } -- path is relative/path/ or /full/path/ or has ..
    | Cat { contentToCat :: [String] } -- contentToCat = [paths or >] 
    | Rm  { files :: [String] }
    | Write { file :: String, content :: String }
    | Invalid deriving (Show, Eq)


-- ################################################################################
-- ## Functions
-- ################################################################################

-- #####################################
-- ## Path calculation
-- #####################################
resolvePath :: String -> [String]
resolvePath path = filter (\h -> h /= ".." && h /= "." && h /= "") $ resolveHlp $ [""] ++ (filter (\x -> length x > 0) $ splitBy '/' path)

resolveHlp :: [String] -> [String]
resolveHlp [] = []
resolveHlp (a:[]) = [a]
resolveHlp (a:"..":c) = resolveHlp c
resolveHlp (a:".":c)  = resolveHlp $ [a] ++ c
resolveHlp (a:b:c)    = [a] ++ (resolveHlp $ [b] ++ c)

calcPath :: String -> String -> [String]
calcPath curr path@('/':last) = resolvePath path
calcPath curr path = resolvePath (curr ++ "/" ++ path)

-- #####################################
-- ## FS get helpers
-- #####################################
getSubfolder :: Folder -> String -> Maybe Folder
getSubfolder fldr subdir 
        |res == []  = Nothing
        |otherwise  = Just (head res)
        where res = filter (\x -> folderName(x) == subdir) $ subFolders(fldr)

getFolder :: FileSystem -> [String] -> Maybe Folder
getFolder fs [] = Just $ root fs
getFolder fs (x:xs) = if (elem x (map (\x-> (folderName x)) $ subFolders $ root fs)) 
                    then getFolder (FileSystem "" (fromJust (getSubfolder (root fs) x))) xs
                    else Nothing

getNewFileSystem :: Folder -> Folder -> [String] -> String -> Maybe FileSystem
getNewFileSystem root currentFolder [] fullPath = Just (FileSystem fullPath root)
getNewFileSystem root currentFolder (x:xs) fullPath = 
    let nextFolder = getSubfolder currentFolder x in
         if (nextFolder == Nothing) then Nothing else getNewFileSystem root (fromJust nextFolder) xs 
         (fullPath ++ "/" ++ folderName(fromJust nextFolder))

-- #####################################
-- ## Parse
-- #####################################
readCommand :: [String] -> Command
readCommand ("pwd":[])    = Pwd
readCommand ("pwd":args)  = Invalid
readCommand ("cd":[])     = Invalid
readCommand ("cd":arg:s)  = Cd arg
readCommand ("ls":[])     = Ls ""
readCommand ("ls":arg:s)  = Ls arg
readCommand ("rm":[])     = Invalid
readCommand ("rm":args)   = Rm args
readCommand ("cat":[])    = Invalid
readCommand ("cat":args)  = Cat args
readCommand ("wr":[])     = Invalid
readCommand ("wr":a:[])   = Invalid
readCommand ("wr":a:b)    = Write a (intercalate " " b)
readCommand _             = Invalid

-- #####################################
-- ## Functions
-- #####################################
-- VERY IMPORTANT DO NOT DELETE
pwd = current -- Can u feel the magic?
-- OKAY..?

splitByHlp :: Char -> String -> String -> [String] -> [String]
splitByHlp c [] [] res      = res
splitByHlp c [] dest res    = res ++ [dest]
splitByHlp c (s:r) dest res = if (s == c)
    then splitByHlp c r "" (res ++ [dest])
    else splitByHlp c r (dest ++ [s]) res

splitBy :: Char -> String -> [String]
splitBy c str = splitByHlp c str "" []


-- ###############
-- ## write
-- ###############
copyOfFoldersCr :: [Folder] -> [String] -> String-> [Folder] 
copyOfFoldersCr [] _ _= []
copyOfFoldersCr (x:xs) path content = (copyOfFolderCr x path content) : (copyOfFoldersCr xs path content)

copyOfFolderCr :: Folder -> [String] -> String -> Folder
copyOfFolderCr x [] content = Folder (folderName x) (copyOfFoldersCr (subFolders x) [] content) (subFiles x) 
copyOfFolderCr x (p:path) content = if (p == folderName x) 
                                    then if (length path == 1 && (not (elem (head path) (map (\y -> fileName y) (subFiles x)))))
                                        then Folder (folderName x) (copyOfFoldersCr (subFolders x) [] content ) ((subFiles x) ++ [File (head path) content] )
                                        else Folder (folderName x) (copyOfFoldersCr (subFolders x) path content) (subFiles x) 
                                    else Folder (folderName x) (copyOfFoldersCr (subFolders x) [] content) (subFiles x)
                                    where includeFile name files content = (filter (\y -> name /= fileName y) files)

write :: FileSystem -> [String] -> String -> CommandResult
write fs path content = CommandResult (FileSystem (current fs) (copyOfFolderCr (root fs) (["/"] ++ path) content)) ""


-- ###############
-- ## rm
-- ###############
copyOfFoldersRm :: [Folder] -> [String] -> [Folder] 
copyOfFoldersRm [] _ = []
copyOfFoldersRm (x:xs) path = copyOfFolderRm x path : copyOfFoldersRm xs path

copyOfFolderRm :: Folder -> [String] -> Folder
copyOfFolderRm x [] = Folder (folderName x) (copyOfFoldersRm (subFolders x) []) (subFiles x) 
copyOfFolderRm x (p:path) = if (p == folderName x) 
                        then if (length path == 1 && (elem (head path) (map (\y -> fileName y) (subFiles x))))
                             then Folder (folderName x) (copyOfFoldersRm (subFolders x) []) (filesWithout (head path) (subFiles x))
                             else Folder (folderName x) (copyOfFoldersRm (subFolders x) path) (subFiles x) 
                        else Folder (folderName x) (copyOfFoldersRm (subFolders x) []) (subFiles x)
                        where filesWithout name files = (filter (\y -> name /= fileName y) files)

rm :: FileSystem -> [String] -> CommandResult
rm fs path = CommandResult (FileSystem (current fs) (copyOfFolderRm (root fs) path)) ""


-- ###############
-- ## cd
-- ###############
cdHelper :: FileSystem -> [String] -> CommandResult
cdHelper fs [] = CommandResult fs ""
cdHelper fs l = let originalRoot = root(fs) 
                in if newStuff == Nothing then CommandResult fs "Incorrect path" else CommandResult (fromJust newStuff) ""
                    where newStuff = (getNewFileSystem (root fs) (root fs) l "")

cd :: FileSystem -> [String] -> CommandResult
cd fs [] = CommandResult (FileSystem "/" (root fs)) ""
cd fs l = cdHelper fs l


-- ###############
-- ## ls
-- ###############
ls :: FileSystem -> [String] -> CommandResult
ls fs [] = CommandResult fs (helpLs1 $ root fs)
ls fs path = if ((getFolder fs path) == Nothing) then CommandResult fs "Nqma takyv path bratched"
            else CommandResult fs (helpLs1 $ fromJust (getFolder fs path))
    
helpLs1 :: Folder -> String
helpLs1 folder = intercalate " " ((map (\x-> (folderName x)) $ subFolders folder) ++ (map (\y-> (fileName y)) $ subFiles folder))


-- ###############
-- ## cat
-- ###############
cat :: FileSystem -> [String] -> CommandResult
cat fs [] = CommandResult fs "Nqma failche seksi"
cat fs path = if ((getFolder fs (take elemsToGet path)) == Nothing) 
            then CommandResult fs "Nqma takyv pyt momche" 
            else if ((getFileCont (fromJust (getFolder fs (take elemsToGet path))) $ last path) == Nothing)
                then CommandResult fs "Nqma takava failche ue"
                else CommandResult fs (fromJust (getFileCont (fromJust (getFolder fs (take elemsToGet path))) $ last path))
                where elemsToGet = (length path) - 1

getFileCont :: Folder -> String -> Maybe String
getFileCont folder name = if (elem name (map (\x -> (fileName x)) $ subFiles folder))
                        then Just $ fileContent $ head (filter (\x -> (fileName x) == name) $ subFiles folder)
                        else Nothing


-- #####################################
-- ## Execute
-- #####################################
executeCat :: [String] -> FileSystem -> String -> CommandResult
executeCat []           fs res = CommandResult fs res
executeCat (">":save:l) fs res = write fs (calcPath (pwd fs) save) res
executeCat (file:rest)  fs res = executeCat rest fs (res ++ output curr)
                                    where curr = cat fs $ calcPath (pwd fs) file

executeRm :: [String] -> FileSystem -> String -> CommandResult
executeRm []       fs res = CommandResult fs res
executeRm (f:args) fs res = executeRm args newFs newRes 
                                where exec   = rm fs (["/"] ++ (calcPath (pwd fs) f)) -- some magic goes here (Ino pilince tuj mi kaza)
                                      newFs  = fileSystem exec
                                      newRes = res ++ (output exec)

execute :: Command -> FileSystem -> CommandResult
execute (Invalid)   fs = CommandResult fs "Invalid command... sorry"
execute (Pwd)       fs = CommandResult fs (pwd fs)
execute (Ls  path)  fs = ls fs $ calcPath (pwd fs) path
execute (Cd  path)  fs = cd fs $ calcPath (pwd fs) path
execute (Write f c) fs = write fs (calcPath (pwd fs) f) c
execute (Rm  args)  fs = executeRm  args fs ""
execute (Cat args)  fs = executeCat args fs ""


-- ################################################################################
-- ## Main Loop
-- ################################################################################
loop :: FileSystem -> IO FileSystem
loop fs = do 
    line <- getLine
    let args = readCommand (words line)
        res = execute args fs
    print (output res)
    loop (fileSystem res)
    --pure $ fs


fs = FileSystem "/" (Folder "/" [
        Folder "l1.d1" [
            Folder "l2.d1" [] [] 
        ] [],
        Folder "l1.d2" [] []
    ] [
        File "l1.f1" "Ba li go"
    ])

--loop fs