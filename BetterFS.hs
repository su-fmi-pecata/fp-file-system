{-# LANGUAGE BlockArguments #-}

import Data.List
import Data.Maybe
import Data.String

-- ################################################################################
-- ## Data types
-- ################################################################################

data File = File {
    fileName    :: String,
    fileContent :: Maybe String
} deriving (Show, Eq)

data FileSystem = FileSystem {
    current :: String,
    files   :: [File]
} deriving (Show)

data CommandResult = CommandResult {
    fileSystem :: FileSystem,
    output     :: String
} deriving (Show)

data Command = Pwd 
    | Append { appFile :: String, what :: String }
    | Cd     { path :: String }
    | Cat    { contentToCat :: [String] }
    | Ls     { dir :: String }
    | Mkdir  { dirPath :: String } 
    | Rm     { filesToRemove :: [String] }
    | Write  { file :: String, content :: String }
    | Invalid deriving (Show, Eq)


-- ################################################################################
-- ## Functions
-- ################################################################################

-- VERY IMPORTANT DO NOT DELETE
pwd = current -- Can u feel the magic?
-- OKAY..?

splitByHlp :: Char -> String -> String -> [String] -> [String]
splitByHlp c [] [] res      = res
splitByHlp c [] dest res    = res ++ [dest]
splitByHlp c (s:r) dest res = if (s == c)
    then splitByHlp c r "" (res ++ [dest])
    else splitByHlp c r (dest ++ [s]) res

splitBy :: Char -> String -> [String]
splitBy c str = splitByHlp c str "" []

-- #####################################
-- ## Path calculation
-- #####################################
resolvePath :: String -> String
resolvePath path = "/" ++ (intercalate "/" $ filter (\h -> h /= ".." && h /= "." && h /= "") $ resolveHlp [] $ [""] ++ (filter (\x -> length x > 0) $ splitBy '/' path))

resolveHlp :: [String] -> [String] -> [String]
resolveHlp res []         = res
resolveHlp res (a:[])     = res ++ [a]
resolveHlp res (a:"..":c) = resolveHlp []           (res ++ c)
resolveHlp res (a:".":c)  = resolveHlp res          ([a] ++ c)
resolveHlp res (a:b:c)    = resolveHlp (res ++ [a]) ([b] ++ c)

calcPath :: String -> String -> String
calcPath curr path@('/':last) = resolvePath path
calcPath curr path = resolvePath (curr ++ "/" ++ path)

-- #####################################
-- ## FS helpers
-- #####################################
getFile :: FileSystem -> String -> Maybe File
getFile fs path = if (1 == length all) then (Just $ all !! 0) else Nothing
                    where all = filter (\x -> path == fileName x) $ files fs

isDirectory :: FileSystem -> String -> Bool
isDirectory fs "/"  = True
isDirectory fs path = d /= Nothing && (fileContent $ fromJust d) == Nothing
                    where d = getFile fs path
                            
isFile :: FileSystem -> String -> Bool
isFile fs path = d /= Nothing && (fileContent $ fromJust d) /= Nothing
                    where d = getFile fs path

removeFilename :: String -> String
removeFilename s = if (length r == 0) then "/" else r
                    where r = take (lastIndexOf '/' s) s

-- #####################################
-- ## Parse
-- #####################################
splitAtFirst :: Eq a => a -> [a] -> ([a], [a])
splitAtFirst x = fmap (drop 1) . break (x ==)

readCommand :: (String, String) -> Command
readCommand ("append",[])      = Invalid
readCommand ("append",args)    = let (a,r) = splitAtFirst ' ' args
                                 in if (length r > 0) then Append a r
                                    else Invalid
readCommand ("cat",[])     = Invalid
readCommand ("cat",args)   = Cat $ words args
readCommand ("cd",[])      = Invalid
readCommand ("cd",args)    = Cd args
readCommand ("ls",[])      = Ls ""
readCommand ("ls",args)    = Ls $ args
readCommand ("mkdir",[])   = Invalid
readCommand ("mkdir",args) = Mkdir args
readCommand ("pwd",[])     = Pwd
readCommand ("pwd",args)   = Invalid
readCommand ("rm",[])      = Invalid
readCommand ("rm",args)    = Rm $ words args
readCommand ("write",[])      = Invalid
readCommand ("write",args)    = let (a,r) = splitAtFirst ' ' args
                                in if (length r > 0) then Write a r
                                   else Invalid
readCommand _             = Invalid

-- #####################################
-- ## Commands
-- #####################################

getFilesByPrefix :: FileSystem -> String -> [File]
getFilesByPrefix fs path = filter (\x -> path `isPrefixOf` (fileName x)) $ files fs


lastIndexOfHlp :: Char -> String -> Int -> Int -> Int
lastIndexOfHlp a []    n r = r
lastIndexOfHlp a (b:c) n r = lastIndexOfHlp a c (n+1) (if (a == b) then n else r)


lastIndexOf :: Char -> String -> Int
lastIndexOf a s = lastIndexOfHlp a s 0 (-1)

-- ###############
-- ## append
-- ###############
append :: FileSystem -> String -> String -> CommandResult
append fs path content
    | isFile fs path = let (Just file) = getFile fs path
                           in CommandResult (FileSystem (current fs) $ (filter (/=file) (files fs))
                            ++ [File path $ Just $ (fromJust $ fileContent file) ++ content]) ""
    | otherwise      = CommandResult fs $ "No such file " ++ path

-- ###############
-- ## cat
-- ###############
cat :: FileSystem -> String -> CommandResult
cat fs file
    | isFile fs file = CommandResult fs $ fromJust $ fileContent $ fromJust $ getFile fs file
    | otherwise      = CommandResult fs $ "No such file " ++ file
    
-- ###############
-- ## cd
-- ###############
cd :: FileSystem -> String -> CommandResult
cd fs path
    | isDirectory fs path = CommandResult (FileSystem path $ files fs) ""
    | otherwise           = CommandResult fs $ "No such dir " ++ path

-- ###############
-- ## ls
-- ###############
ls :: FileSystem -> String -> CommandResult
ls fs path
    | isDirectory fs path = CommandResult fs $ intercalate "\n" $ map fileName $ filter (\x -> (lastIndexOf '/' $ fileName x) == ind) $ getFilesByPrefix fs path
    | otherwise           = CommandResult fs $ "No such dir " ++ path
        where ind = lastIndexOf '/' $ if ('/' == last path) then path else path ++ "/"

-- ###############
-- ## mkdir
-- ###############
mkdir :: FileSystem -> String -> CommandResult
mkdir fs path
    | hasDir && file == Nothing = CommandResult (FileSystem (current fs) $ (files fs) ++ [File path Nothing]) ""
    | hasDir                    = CommandResult fs $ "Already exist " ++ path
    | otherwise                 = CommandResult fs $ "Parent " ++ parent ++ " is not dir"
        where parent = removeFilename path
              hasDir = isDirectory fs parent
              file   = getFile fs path

-- ###############
-- ## rm
-- ###############
rm :: FileSystem -> String -> CommandResult
rm fs path
    | isFile fs path = CommandResult (FileSystem (current fs) (filter (/= (fromJust $ getFile fs path)) $ files fs)) ""
    | otherwise      = CommandResult fs $ "No such file " ++ path

-- ###############
-- ## wr
-- ###############
write :: FileSystem -> String -> String -> CommandResult
write fs path content
    | hasDir && file == Nothing
       || hasDir && Nothing /= (fileContent $ fromJust file)
                       = CommandResult (FileSystem (current fs) $ (filter (\x -> path /= fileName x) (files fs)) ++ [File path $ Just content]) ""
    | hasDir           = CommandResult fs $ "Already exist such dir.. sorry " ++ path
    | otherwise        = CommandResult fs $ "Parent " ++ parent ++ " is not dir"
        where parent = removeFilename path
              hasDir = isDirectory fs parent
              file   = getFile fs path

-- #####################################
-- ## Execute
-- #####################################
executeCat :: [String] -> FileSystem -> String -> CommandResult
executeCat []           fs res = CommandResult fs res
executeCat (">":save:l) fs res = write fs (calcPath (pwd fs) save) res
executeCat (file:rest)  fs res = executeCat rest fs (res ++ output curr)
                                    where curr = cat fs $ calcPath (pwd fs) file

executeRm :: [String] -> FileSystem -> String -> CommandResult
executeRm []       fs res = CommandResult fs res
executeRm (f:args) fs res = executeRm args newFs newRes 
                                -- where exec   = rm fs (["/"] ++ (calcPath (pwd fs) f)) -- some magic goes here (Ino pilince tuj mi kaza)
                                where exec   = rm fs (calcPath (pwd fs) f)
                                      newFs  = fileSystem exec
                                      newRes = res ++ (output exec)

execute :: Command -> FileSystem -> CommandResult
execute (Invalid )   fs = CommandResult fs "Invalid command... sorry"
execute (Append f c) fs = append fs (calcPath (pwd fs) f) c
execute (Cat args )  fs = executeCat args fs ""
execute (Cd  path)   fs = cd fs $ calcPath (pwd fs) path
execute (Ls  path)   fs = ls fs $ calcPath (pwd fs) path
execute (Mkdir  d)   fs = mkdir fs $ calcPath (pwd fs) d
execute (Pwd)        fs = CommandResult fs (pwd fs)
execute (Rm  args)   fs = executeRm  args fs ""
execute (Write f c)  fs = write fs (calcPath (pwd fs) f) c


-- ################################################################################
-- ## Main Loop
-- ################################################################################
loop :: FileSystem -> IO FileSystem
loop fs = do 
    line <- getLine
    let args = readCommand (splitAtFirst ' ' line)
        res = execute args fs
    -- print (args)
    putStrLn (output res)
    loop (fileSystem res)
    --pure $ fs


fs = FileSystem "/" [
        File "/file" (Just "File content /file"),
        File "/dir" Nothing,
        File "/d12" Nothing,
        File "/dir/dir2" Nothing,
        File "/dir/d2" Nothing,
        File "/dir/file" (Just "File content /dir/file")
    ]